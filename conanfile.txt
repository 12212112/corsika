[requires]
spdlog/1.9.2
catch2/2.13.8
eigen/3.3.8
boost/1.76.0
zlib/1.2.11
proposal/7.3.1
yaml-cpp/0.6.3
arrow/2.0.0
cli11/1.9.1

[build_requires]
readline/8.0 # this is only needed to fix a missing dependency in "bison" which is pulled-in by arrow
bison/[>1.0] # needed for arrow, and we HAVE to compile it

[generators]
cmake

[options]
readline:shared=True
arrow:shared=False
arrow:parquet=True
arrow:fPIC=False
arrow:with_re2=False
arrow:with_protobuf=False
arrow:with_openssl=False
arrow:with_gflags=False
arrow:with_glog=False
arrow:with_grpc=False
arrow:with_utf8proc=False
arrow:with_zstd=False
arrow:with_bz2=False
