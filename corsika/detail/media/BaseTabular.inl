/*
 * (c) Copyright 2020 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

#pragma once

#include <corsika/framework/core/ParticleProperties.hpp>
#include <corsika/framework/core/PhysicalUnits.hpp>
#include <corsika/framework/geometry/Point.hpp>

#include <exception>

/* 
Change log
1. redefine height to be relative hight in getMassDensity
2. rewrite getGrammage so that it can go up and down
3. rewrite getLength so that it can go up and down
4. getArcLength must be able to work with overlarge grammage
TODO
1. check if track will go out of BaseTabular, it will happen if track length if not accurate
2. getGrammage should assert if the track is too long
*/

namespace corsika {

  template <typename TDerived>
  inline BaseTabular<TDerived>::BaseTabular(
      Point const& point, LengthType const referenceHeight,
      std::function<MassDensityType(LengthType)> const& rho, unsigned int const nBins,
      LengthType const deltaHeight)
      : nBins_(nBins)
      , deltaHeight_(deltaHeight)
      , point_(point)
      , referenceHeight_(referenceHeight) {
    density_.resize(nBins_);
    for (unsigned int bin = 0; bin < nBins; ++bin) {
      density_[bin] = rho(deltaHeight_ * bin);
      CORSIKA_LOG_DEBUG("new tabulated atm bin={} h={} rho={}", bin, deltaHeight_ * bin,
                        density_[bin]);
    }
  }

  template <typename TDerived>
  inline auto const& BaseTabular<TDerived>::getImplementation() const {
    return *static_cast<TDerived const*>(this);
  }

  template <typename TDerived>
  inline MassDensityType BaseTabular<TDerived>::getMassDensity(
      LengthType const height) const {
    double const fbin = height / deltaHeight_;
    int const bin = int(fbin);
    if (bin < 0) return MassDensityType::zero();
    if (bin >= int(nBins_ - 1)) {
      CORSIKA_LOG_ERROR(
          "invalid height {} (corrected {}) in BaseTabular atmosphere. Min 0, max {}. If "
          "max is too low: increase!",
          height, height + referenceHeight_, nBins_ * deltaHeight_);
      throw std::runtime_error("invalid height");
    }
    return density_[bin] + (fbin - bin) * (density_[bin + 1] - density_[bin]);
  }

  template <typename TDerived>
  inline GrammageType BaseTabular<TDerived>::getIntegratedGrammage(
      BaseTrajectory const& traj) const {
    // TODO check if out of bin
    GrammageType X(GrammageType::zero());
    LengthType dD(LengthType::zero());
    LengthType lengthFull = traj.getLength(1);
    LengthType lengthToGo = lengthFull;
    auto PosRelative = traj.getPosition(0) - point_;
    LengthType PosCosTheta = PosRelative.dot(traj.getDirection(0));
    LengthType heightInit = PosRelative.getNorm() - referenceHeight_;
    MassDensityType densityCurr = getMassDensity(heightInit);
    MassDensityType densityNext(MassDensityType::zero());
    MassDensityType density(MassDensityType::zero());
    LengthType radiusNext(LengthType::zero());
    int binCurr = static_cast<int>(heightInit / deltaHeight_);
    int binNext = 0;
    double frac = 0.;
    int sign = PosCosTheta >= LengthType::zero() ? 1 : -1;
    bool first_bin = true;

    while (true) {
      binNext = binCurr + (sign > 0 ? 1 : (first_bin ? 0 : -1));
      radiusNext = deltaHeight_ * binNext + referenceHeight_;
      auto dDsquare1 =
          square(radiusNext) - PosRelative.dot(PosRelative) + square(PosCosTheta);
      if (dDsquare1.magnitude() < 0) {
        CORSIKA_LOG_DEBUG("Go back!");
        sign *= -1;
        binNext = binCurr + (sign > 0 ? 1 : (first_bin ? 0 : -1));
        radiusNext = deltaHeight_ * binNext + referenceHeight_;
        dDsquare1 =
          square(radiusNext) - PosRelative.dot(PosRelative) + square(PosCosTheta);
      }
      if (binNext > static_cast<int>(nBins_ - 1) || binNext < 0) {
        CORSIKA_LOG_ERROR("invalid bin {} encountered in BaseTabular integration",
                          binNext);
        throw std::runtime_error("invalid height");
      }
      dD = sign * sqrt(dDsquare1) - PosCosTheta;
      if (dD > lengthToGo) {
        LengthType heightFinal = (traj.getPosition(1) - point_).getNorm() - referenceHeight_;
        densityNext = getMassDensity(heightFinal);
        return X + (densityCurr + densityNext) / 2 * lengthToGo;
      } else {
        densityNext = density_[binNext];
        density = (densityCurr + densityNext) / 2;
        lengthToGo -= dD;
        X += density * dD;
      }
      {  // trace info here
        const auto &cs = traj.getPosition(0).getCoordinateSystem();
        auto initPos = traj.getPosition(frac);
        auto initDir = traj.getDirection(frac);
        CORSIKA_LOG_TRACE("binCurr: {:d}, binNext: {:d}", binCurr, binNext);
        CORSIKA_LOG_TRACE("frac: {:.4f}, lengthToGo: {:.2f}, fullLength: {:.2f}",
          frac, lengthToGo / 1_m, lengthFull / 1_m);
        CORSIKA_LOG_TRACE("initial position: ({:.2f}, {:.2f}, {:.2f}) m",
                          initPos.getX(cs) / 1_m, initPos.getY(cs) / 1_m,
                          initPos.getZ(cs) / 1_m);
        CORSIKA_LOG_TRACE("initial direction: ({}, {}, {})",
                          initDir.getX(cs), initDir.getY(cs), initDir.getZ(cs));
      }
      binCurr = binNext;
      frac = (lengthFull - lengthToGo) / lengthFull;
      PosRelative = traj.getPosition(frac) - point_;
      PosCosTheta = PosRelative.dot(traj.getDirection(frac));
      densityCurr = densityNext;
      first_bin = false;
    }
  }

  template <typename TDerived>
  inline LengthType BaseTabular<TDerived>::getArclengthFromGrammage(
      BaseTrajectory const& traj, GrammageType const grammage) const {

    if (grammage < GrammageType::zero()) {
      CORSIKA_LOG_ERROR("cannot integrate negative grammage");
      throw std::runtime_error("negative grammage error");
    }
    LengthType const height = (traj.getPosition(0) - point_).getNorm() - referenceHeight_;

    double const fbin = height / deltaHeight_;
    int bin = static_cast<int>(fbin);

    if (bin >= static_cast<int>(nBins_ - 1) || bin < 0) {
      CORSIKA_LOG_ERROR("invalid height {} in BaseTabular atmosphere integration",
                        height);
      throw std::runtime_error("invalid height");
    }

    // interpolated start/end densities
    MassDensityType const rho = getMassDensity(height);

    // inclination of trajectory
    Point pCurr = traj.getPosition(0);
    DirectionVector dCurr = traj.getDirection(0);
    DirectionVector axis((pCurr - point_).normalized());
    double cosTheta = axis.dot(dCurr);
    int sign = +1; // height increasing along traj
    if (cosTheta < 0) {
      cosTheta = -cosTheta; // absolute value only
      sign = -1;            // height decreasing along traj
    }

    // start with 0 g/cm2
    GrammageType X(GrammageType::zero());
    LengthType distance(LengthType::zero());
    LengthType dD(LengthType::zero());
    LengthType const fullLength = traj.getLength(1);
    double frac;

    // within first bin
    auto PosRelative = pCurr - point_;
    LengthType PosCosTheta = PosRelative.dot(dCurr);
    LengthType nextRadius = deltaHeight_ * (bin + (sign > 0 ? 1 : 0)) + referenceHeight_;
    auto dDsquare1 =
        square(nextRadius) - PosRelative.dot(PosRelative) + square(PosCosTheta);
    if (dDsquare1.magnitude() < 0) {
      // no intersection with next sphere, meaning the track is going back
      CORSIKA_LOG_DEBUG("Go back");
      sign *= -1;
      LengthType nextRadius =
          deltaHeight_ * (bin + (sign > 0 ? 1 : 0)) + referenceHeight_;
      dDsquare1 = square(nextRadius) - PosRelative.dot(PosRelative) + square(PosCosTheta);
    }
    dD = sign * sqrt(dDsquare1) - PosCosTheta;
    auto densityCurr = (rho + density_[bin + (sign > 0 ? 1 : 0)]) / 2;
    GrammageType binGrammage = dD * densityCurr;
    distance += dD;
    frac = distance / fullLength;
    if (X + binGrammage > grammage) {
      double const binFraction = (grammage - X) / binGrammage;
      return distance * binFraction;
    }
    X += binGrammage;
    CORSIKA_LOG_TRACE("First, bin: {:d}, distance: {:.2f}, dD: {:.2f}, density: {:.2f}",
                      bin, distance / 1_cm, dD / 1_cm, densityCurr / (1_g / cube(1_cm)));

    // the following bins (along trajectory)
    for (bin += sign; bin < static_cast<int>(nBins_) && bin >= 0; bin += sign) {
      pCurr = traj.getPosition(frac);
      dCurr = traj.getDirection(frac);
      PosRelative = pCurr - point_;
      nextRadius += sign * deltaHeight_;
      PosCosTheta = PosRelative.dot(dCurr);
      dDsquare1 = square(nextRadius) - PosRelative.dot(PosRelative) + square(PosCosTheta);
      if (dDsquare1.magnitude() < 0) {
        // no intersection with next sphere, meaning the track is going back
        CORSIKA_LOG_DEBUG("Go back");
        sign *= -1;
        continue;
      }
      dD = sign * sqrt(dDsquare1) - PosCosTheta;
      auto densityCurr = (density_[bin] + density_[bin + 1]) / 2;
      binGrammage = dD * densityCurr;
      if (X + binGrammage > grammage) {
        double const binFraction = (grammage - X) / binGrammage;
        auto result = distance + dD * binFraction;
        CORSIKA_LOG_TRACE(
            "distance: {:.2f}, dD: {:.4f}, binFraction: {:.4f}, rst: {:.4f}",
            distance / 1_cm, dD / 1_cm, binFraction, result / 1_cm);
        return result;
      }
      X += binGrammage;
      distance += dD;
      frac = distance / fullLength;
      auto hCurr = PosRelative.getNorm() - referenceHeight_;
      CORSIKA_LOG_DEBUG(
          "hCurr: {:.2f}, bin: {:d}, distance: {:.2f}, X: {:.2f}, dD: {:.4f}, density: "
          "{:.4f}",
          hCurr / 1_cm, bin, distance / 1_cm, X / (1_g / square(1_cm)), dD / 1_cm,
          densityCurr / (1_g / cube(1_cm)));
    }
    return std::numeric_limits<double>::infinity() * meter;
  }

} // namespace corsika
