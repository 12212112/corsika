/**
 * \file quantity_io_symbols.hpp
 *
 * \brief   load all available unit names and symbols.
 * \author  Martin Moene
 * \date    7 September 2013
 * \since   1.0
 *
 * Copyright 2013 Universiteit Leiden. All rights reserved.
 * This code is provided as-is, with no warrantee of correctness.
 *
 * Distributed under the Boost Software License, Version 1.0. (See accompanying
 * file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef PHYS_UNITS_QUANTITY_IO_SYMBOLS_HPP_INCLUDED
#define PHYS_UNITS_QUANTITY_IO_SYMBOLS_HPP_INCLUDED

#include "corsika/framework/units/quantity_io_ampere.hpp"
//prefer Hertz
//#include "corsika/framework/units/quantity_io_becquerel.hpp"
#include "corsika/framework/units/quantity_io_candela.hpp"
//prefer kelvin
//#include "corsika/framework/units/quantity_io_celsius.hpp"
#include "corsika/framework/units/quantity_io_coulomb.hpp"
#include "corsika/framework/units/quantity_io_dimensionless.hpp"
#include "corsika/framework/units/quantity_io_farad.hpp"
//prefer sievert
//#include "corsika/framework/units/quantity_io_gray.hpp"
#include "corsika/framework/units/quantity_io_joule.hpp"
#include "corsika/framework/units/quantity_io_henry.hpp"
#include "corsika/framework/units/quantity_io_hertz.hpp"
#include "corsika/framework/units/quantity_io_kelvin.hpp"
#include "corsika/framework/units/quantity_io_kilogram.hpp"
//prefer Cd base unit
//#include "corsika/framework/units/quantity_io_lumen.hpp"
#include "corsika/framework/units/quantity_io_lux.hpp"
#include "corsika/framework/units/quantity_io_meter.hpp"
#include "corsika/framework/units/quantity_io_newton.hpp"
#include "corsika/framework/units/quantity_io_ohm.hpp"
#include "corsika/framework/units/quantity_io_pascal.hpp"
#include "corsika/framework/units/quantity_io_radian.hpp"
#include "corsika/framework/units/quantity_io_second.hpp"
#include "corsika/framework/units/quantity_io_siemens.hpp"
#include "corsika/framework/units/quantity_io_sievert.hpp"
#include "corsika/framework/units/quantity_io_speed.hpp"
#include "corsika/framework/units/quantity_io_steradian.hpp"
#include "corsika/framework/units/quantity_io_tesla.hpp"
#include "corsika/framework/units/quantity_io_volt.hpp"
#include "corsika/framework/units/quantity_io_watt.hpp"
#include "corsika/framework/units/quantity_io_weber.hpp"

#endif // PHYS_UNITS_QUANTITY_IO_SYMBOLS_HPP_INCLUDED

/*
 * end of file
 */
