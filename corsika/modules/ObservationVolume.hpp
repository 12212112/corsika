/*
 * (c) Copyright 2022 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

#pragma once

#include <corsika/modules/writers/ObservationVolumeWriterParquet.hpp>
#include <corsika/framework/process/ContinuousProcess.hpp>

namespace corsika {

  /**
     @ingroup Modules
     @{

     The ObservationVolume writes PDG codes, energies, and distances of particles to the
     central point of the plane into its output file. The particles are considered
     "absorbed" afterwards.

     **Note/Limitation:** as discussed in
     https://gitlab.ikp.kit.edu/AirShowerPhysics/corsika/-/issues/397
     you cannot put two ObservationVolumes exactly on top of each
     other. Even if one of them is "permeable". You have to put a
     small gap in between the two plane in such a scenario, or develop
     another more specialized output class.
   */
  template <typename TTracking, typename TVolume, typename TOutputWriter = ObservationVolumeWriterParquet>
  class ObservationVolume
      : public ContinuousProcess<ObservationVolume<TTracking, TVolume, TOutputWriter>>,
        public TOutputWriter {

  public:
    ObservationVolume(CoordinateSystemPtr cs, TVolume vol, bool = true);

    template <typename TParticle, typename TTrajectory>
    ProcessReturn doContinuous(TParticle& vParticle, TTrajectory& vTrajectory,
                               bool const stepLimit);

    template <typename TParticle, typename TTrajectory>
    LengthType getMaxStepLength(TParticle const&, TTrajectory const& vTrajectory);

    void showResults() const;
    void reset();
    HEPEnergyType getEnergy() const { return energy_; }
    YAML::Node getConfig() const;

  private:
    TVolume vol_;
    bool const deleteOnHit_;
    HEPEnergyType energy_;
    unsigned int count_;
  };
  //! @}
} // namespace corsika

#include <corsika/detail/modules/ObservationVolume.inl>