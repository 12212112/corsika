/*
 * (c) Copyright 2020 CORSIKA Project, corsika-project@lists.kit.edu
 *
 * This software is distributed under the terms of the GNU General Public
 * Licence version 3 (GPL Version 3). See file LICENSE for a full version of
 * the license.
 */

#pragma once

// the location of the "xmldoc" pythia directory (data files, config)
#include <corsika/modules/pythia8/Pythia8ConfigurationDirectory.hpp>

// the main pythia include
#include <Pythia8/Pythia.h>
