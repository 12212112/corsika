Tracking
========

Tracking is the process of moving a kinetic object from one point A to another point B. 
Physical forces and laws should be applied explicitly or numerically in this process. 

There are two main aspects of tracking:

1. The actual proposal of a new point B, thus, a movement. 
2. The description of the movement within a 3D geometry with boundaries and different volumina.

Besides this, there furthermore is also the determination of the effects of the
movement (e.g. magnetic fields, refractivity), or the accumulation of local 
quantities (e.g. grammage). 
For these purposes typically numerical integration routines are needed. 


