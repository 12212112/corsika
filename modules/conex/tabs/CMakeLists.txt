# Distribution of partly compressed data tables+files for air shower physics

cmake_minimum_required (VERSION 3.9)
project (CorsikaData)

get_directory_property (hasParent PARENT_DIRECTORY)
if (hasParent)
  set (CorsikaData_DIR ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)
  set (CorsikaData_VERSION "1.1.0" PARENT_SCOPE)
endif (hasParent)

find_package (BZip2 REQUIRED)

enable_testing ()
if (${CMAKE_VERSION} VERSION_LESS "3.12.0")  
  list (APPEND CMAKE_CTEST_ARGUMENTS "--output-on-failure")
else (${CMAKE_VERSION} VERSION_LESS "3.12.0")
  set (CTEST_OUTPUT_ON_FAILURE 1) # this is for new versions of cmake
endif (${CMAKE_VERSION} VERSION_LESS "3.12.0")

# this may be included in our project to access corsika-data by adding
# add_subdirectory (corsika-data)
# to your CMakeLists.txt
# it creates the cmake taget "CorsikaData"
# and it builds the libCorsikaData.a archive.

add_subdirectory (readLib)
